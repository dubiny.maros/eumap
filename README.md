# EU MAP

This project was created for subject `FI:PV251 Visualization` at Masaryk University. 
It is capable of displaying data from  [kaggle's europe dataset](https://www.kaggle.com/roshansharma/europe-datasets)
as well as any CSV file in same format
in a form of a color scaled map and bar chart, as well as switching between red, green and blue color scale.
JavaScript libraries used here are d3 and chart.js. 

For the purpose of traversing file system and formatting data to be compatible with downloaded SVG map, 
I've created a simple python:flask backend runnable by gunicorn. 

App is hosted [here](https://eumap.herokuapp.com/#), with help of heroku hosting system.

#### To run backend locally:
* install python interpreter
* install [virtualenv](https://virtualenv.pypa.io/en/latest/) or 
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) + create and switch to environment (optional)
* install dependencies with pip: `pip install -r requirements.txt`
* run the [gunicorn](http://docs.gunicorn.org/en/stable/run.html)
 server `gunicorn backend:app` (you can specify port/socket by parameter `-b`)