import csv
import os

from flask import Flask, render_template, json, request, abort, send_from_directory
from flask_cors import CORS

from config import COUNTRY_ID_TO_ID

app = Flask(__name__)
CORS(app)

def column_to_name(column):
    return column.replace("_", " ").upper()


def name_to_column(name):
    return name.replace(" ", "_").lower()


def file_to_name(file):
    return column_to_name(file[:-4])


def name_to_file(name):
    return name_to_column(name) + ".csv"


@app.route('/ping/')
def hello_world():
    return 'pong'


@app.route('/datasets/')
def list_datasets():
    files = os.listdir("./datasets")
    return json.dumps({"datasets": [file_to_name(f) for f in files]})


@app.route('/metrics/')
def list_metrics():
    dataset = name_to_file(request.args['dataset']) if request.args.get('dataset') else ""
    path = "./datasets/{}".format(dataset)
    if not os.path.isfile(path):
        abort()
    with open(path) as f:
        reader = csv.reader(f)
        metrics = next(reader, None)[1:]
    return json.dumps({"metrics": [column_to_name(m) for m in metrics]})


@app.route('/data/')
def list_data():
    dataset = name_to_file(request.args['dataset']) if request.args.get('dataset') else ""
    path = "./datasets/{}".format(dataset)
    if not os.path.isfile(path):
        abort()
    with open(path) as f:
        reader = csv.reader(f)
        metrics = next(reader, [])
        if not metrics or not name_to_column(request.args.get('metric')) in metrics:
            abort()
        data_index = metrics.index(name_to_column(request.args["metric"]))
        data = {}
        for row in reader:
            if COUNTRY_ID_TO_ID.get(row[0]):
                data[COUNTRY_ID_TO_ID[row[0]]] = row[data_index]
    return json.dumps({"data": data})


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run(debug=True)
