let theme = "green";
let dataset_button = document.getElementById('dataset');
let metrics_button = document.getElementById('metrics');
let dataset_button_content = document.getElementById('dataset-content');
let metrics_button_content = document.getElementById('metrics-content');
let dataset_value = null;
let metric_value = null;
let chart = null;
let svg = document.getElementById('svg');
let selected_country = null;

GetDatasets();

const pallet = {
    green: "#006407",
    blue: "#0d3faf",
    red: "#af0400",
};


function DeleteBorders(country) {
    let map_country = document.getElementById(country);
    map_country.style.border = null;
}


function SelectCountry(country){
    if (selected_country !== null){
        DeleteBorders(selected_country)
    }
    selected_country = country;
    let map_country = document.getElementById(selected_country);
    map_country.style.border = "2px solid black;";
}

function AddOnClick(data) {
    for (let country in data)
        addEventListener("click", () => SelectCountry(country))
}

function SortData(data) {
    let sortable = [];
    for (let k in data) {
        sortable.push([k, data[k]]);
    }
    sortable.sort(function (a, b) {
        return b[1] - a[1];
    });
    return sortable;
}


function CreateChart(data) {
    let sorted = SortData(data);
    let values = [];
    let keys = [];
    for (let i = 0, size = sorted.length; i < size; i++) {
        keys.push(sorted[i][0]);
        values.push(sorted[i][1]);
    }
    DrawChart(keys, values)
}

function DrawChart(keys, values) {
    let ctx = document.getElementById('myChart').getContext('2d');
    if (chart !== null) {
        chart.destroy()
    }
    chart = new Chart(ctx, {
        type: 'horizontalBar',
        position: "right",
        data: {
            labels: keys,
            datasets: [{
                label: dataset_value + " " + metric_value,
                data: values,
                backgroundColor: pallet[theme],
                borderColor: pallet[theme],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        stacked: true
                    },
                    position: 'right',
                }],
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    reverse: true,
                    ticks: {
                        beginAtZero: true,
                        reverse: true
                    },
                }]
            }
        }
    });
}

function SetTheme(color) {
    let stylesheet = document.getElementById('stylesheet');
    stylesheet.setAttribute("href", "../static/css/" + color + ".css");
    theme = color;
    if (metric_value !== null) {
        OnMetricsClick(metric_value, dataset_value)
    }
}

function GetColorScale(data) {
    let values = Object.keys(data).map(function (key) {
        return data[key];
    });
    let max = Math.max.apply(0, values);
    let min = Math.min.apply(0, values);
    return d3.scaleLinear().domain([min, max]).range(["white", theme]);
}


function DrawMap(data) {
    let color_scale = GetColorScale(data);
    for (let country_name in data) {
        let country = document.getElementById(country_name);
        country.style.fill = color_scale(data[country_name]);
    }
}


function DrawVisualizations(data) {
    AddOnClick(data);
    DrawMap(data);
    CreateChart(data)
}


function OnMetricsClick(metric, dataset) {
    metrics_button.innerText = metric;
    metric_value = metric;
    dataset_value = dataset;
    fetch("/data/" + "?dataset=" + dataset + "&metric=" + metric
    ).then(response => response.json().then(
        data => DrawVisualizations(data.data)
    ))
}

function PopulateMetrics(data, dataset) {
    let metrics_options = data.metrics;
    while (metrics_button_content.hasChildNodes()) {
        metrics_button_content.removeChild(metrics_button_content.firstChild);
    }
    for (let i = 0; i < metrics_options.length; i++) {
        let opt = metrics_options[i];
        let el = document.createElement('a');
        el.textContent = opt;
        el.value = opt;
        el.href = '#';
        el.addEventListener('click', () => OnMetricsClick(opt, dataset));
        metrics_button_content.appendChild(el);
    }
}

function OnDatasetClick(dataset) {
    dataset_button.innerText = dataset;
    dataset_value = dataset;
    metrics_button.innerText = "CHOOSE METRIC";
    metric_value = null;
    if (chart !== null) {
        chart.destroy()
    }
    for (let i = 0, size = svg.children.length; i < size; i++) {
        svg.children[i].style.fill = "#ececec";
    }
    chart = null;
    fetch("/metrics/" + "?dataset=" + dataset
    ).then(response => response.json().then(
        data => PopulateMetrics(data, dataset)
    ))
}

function PopulateDatasets(data) {
    let dataset_options = data.datasets;
    for (let i = 0; i < dataset_options.length; i++) {
        let opt = dataset_options[i];
        let el = document.createElement('a');
        el.textContent = opt;
        el.value = opt;
        el.href = '#';
        el.addEventListener('click', () => OnDatasetClick(opt));
        dataset_button_content.appendChild(el);
    }
}


function GetDatasets() {
    fetch("/datasets/",
    ).then(response => response.json().then(
        data => PopulateDatasets(data)
    )).catch(e => {
        console.log(e);
    });
}

